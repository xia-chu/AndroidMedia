package com.wing.tools.video;

/**
 * Created by wejust on 2017/12/6.吗
 */

public class ZLMediaKit {
    public static native int initRtspServer(short port);
    public static native int initRtmpServer(short port);
    public static native int initHttpServer(short port);
    public static native long createMedia(String appName, String mediaName);
    public static native void setGlobalOptionString(String key, String value);
    public static native void releaseMedia(long ctx);
    public static native void mediaInitVideo(long ctx, int width, int height, int frameRate);
    public static native void mediaInitAudio(long ctx, int channel, int sampleBit, int sampleRate);
    public static native void mediaInputH264(long ctx, byte[] data, int len, long stamp, boolean allinone);
    public static native void mediaInputAAC(long ctx, byte[] data, int len,long stamp);
    public static native String getLocalIp();

    public static native String getUrl(int type,String app,String stream);
    public static native int getBitRate(int width,int height,int fps);
    public static native int waitForEvent(int millisec);
    public static native void stopEventWait();
    public static native String getEventInfo(int opt);

    public static native void reloadConfig();
    public static native void createPath(String path);
    public static native void startMvManager();

    public static native long createProxyPlayer(String app,String stream,int type);
    public static native void releaseProxyPlayer(long ctx);
    public static native void proxyPlayerPlay(long ctx,String url);

    public static class Proxy{
        private long contex = 0;
        public Proxy(String app,String stream,int type){
            contex = createProxyPlayer(app,stream,type);
        }
        public void release(){
            if(contex != 0 ){
                releaseProxyPlayer(contex);
                contex = 0;
            }
        }
        public void play(String url){
            if(contex != 0 ){
                proxyPlayerPlay(contex,url);
                contex = 0;
            }
        }
        @Override
        protected void finalize() throws Throwable {
            super.finalize();
            release();
        }
    }

    static {
        System.loadLibrary("mediajni");
    }
}
