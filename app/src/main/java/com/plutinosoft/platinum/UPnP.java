package com.plutinosoft.platinum;

import java.util.Map;

public class UPnP {
    private long mDMCHandle;
    private static UPnP instance = null;
    public static UPnP Instance(){
          if(instance == null){
              instance = new UPnP();
          }
          return instance;
    }
    public UPnP() {
        cSelf = _init();
    }

    public int start() {
        return _start(cSelf);
    }
    
    public int stop() {
        return _stop(cSelf);
    }

    public void startDMC(){
        mDMCHandle = _create_dmc(cSelf);
    }

    public void stopDMC(){
        _delete_dmc(mDMCHandle);
    }

    public Map<String,String> getAllDMR(){
        return _get_all_dmr(mDMCHandle);
    }

    public boolean setDMR(String uuid){
        return _set_dmr(mDMCHandle, uuid);
    }

    public String getMDR(){
        return _get_dmr(mDMCHandle);
    }

    public boolean dmrOpen(String uri,String metadata){
        return  _open_dmr(mDMCHandle, uri, metadata);
    }

    public boolean dmrPlay(){
        return _play_dmr(mDMCHandle);
    }

    public boolean dmrStop(){
        return _stop_dmr(mDMCHandle);
    }

    // C glue
    private static native long _init();
    private static native int _start(long self);
    private static native int _stop(long self);
    private final long     cSelf;

    private static native long _create_dmc(long self);
    private static native void _delete_dmc(long dmc);
    private static native Map<String,String> _get_all_dmr(long dmc);
    private static native boolean _set_dmr(long dmc, String uuid);
    private static native String _get_dmr(long dmc);
    private static native boolean _open_dmr(long dmc, String uri, String metadata);
    private static native boolean _play_dmr(long dmc);
    private static native boolean _stop_dmr(long dmc);

    static {
        System.loadLibrary("platinum-jni");
    }
}
