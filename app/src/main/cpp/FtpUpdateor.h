//
// Created by xzl on 2018/2/21.
//

#ifndef FTPDOWNLOADERIMP_H
#define FTPDOWNLOADERIMP_H

#include "FtpDownloader.h"
#include "Util/File.h"
#include "Util/mini.h"
#include "Util/TimeTicker.h"

//临时文件日期
#define UPDATE_KEY_FILE_TIME_TMP "update.file_time_tmp"
//文件日期
#define UPDATE_KEY_FILE_TIME "update.file_time"

using namespace ZL::Util;

class FtpUpdateor : public FtpDownloader  {
public:
    typedef std::shared_ptr<FtpUpdateor> Ptr;
    typedef function<void(int code,const string &errMsg)> onResult;
    typedef function<void(int fileSize,int fileTime,int code,const string &errMsg)> onGetFileSize;

    FtpUpdateor(const string &user_name = "",const string &passwd = "");
    virtual ~FtpUpdateor();

    //更新文件
    void update(const string &url,const string &filePath,const onResult &cb);
    //下载文件
    //如果resume = true，那么将不考虑远程文件是否更新而继续下载，
    //如果resume = false，将重新下载
    void download(const string &url,const string &filePath,bool resume,const onResult &cb);
    //获取文件大小
    void getRemoteFileInfo(const string &url,const onGetFileSize &cb);
protected:
    void onStart(int total,int offset,int fileTime) override; //开始下载
    void onCancel() override;//被取消
    void onProgress(char *data,int size) override;//下载中...
    void onComplete() override;//下载成功
    void onFailed(int err_code,const string &err_msg) override;//下载中途失败
private:
    int getFileSize(const string &filePath);
private:
    std::shared_ptr<FILE> _file;
    FtpCallBack _callback;
    Ticker _speedTicker;
};


#endif //FTPDOWNLOADERIMP_H
